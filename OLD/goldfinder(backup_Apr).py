#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python script to subselect the best markers produced by the output of MarkerMiner
# Written by Oscar M. Vargas oscarmvargas.org

import argparse
import os
from Bio import SeqIO
from Bio import AlignIO
import pandas as pd
import re
import glob
import shutil
import time
import sys

####### Arguments and help ###########
parser = argparse.ArgumentParser(description="\
Script to subselect the best markers produced by the output of MarkerMiner, \
to run the program you need to run it from the the folder with the results of MarkerMiner:\
 the folder containing single_copy_genes.txt and all the folders with aligments. \
Runing the code without arguments will run the program with all the defaults. \
The program will always overwrite your results\
")
parser.add_argument("-bl", type=int, help="length of baits, default=120", default=120)
parser.add_argument("-bc", type=int, help="converage of baits, default=2", default=2)
parser.add_argument("-ml", type=int, help="minimum lenght of marker, default=900", default=900)
parser.add_argument("-pse", type=int, help="percentage of short exons allowed, default=30", default=30)
parser.add_argument("-ns", type=int, help="minimum number of samples with sequences in marker aligment, default=2", default=2)
parser.add_argument("-ms", type=int, help="minimum (percentage) similarity among sequences in alignment, default=60", default=60)
parser.add_argument("-nb", type=int, help="number of baits desired, default=30000", default=30000)
parser.add_argument("-r", help="reference used in MarkerMiner, default=AT, available: AL,AT,BD,CP,FV,GM,MD,ME,MT,OS,PT,RC,SB,TC,VV,ZM. If you used your own reference, plese add the two first letters found in every name sequence in your reference", default="AT")
parser.parse_args()
args = parser.parse_args() 

bait_length = args.bl
bait_coverage = args.bc
minimum_marker_lenght = args.ml
perc_short_exons_desired = args.pse
seq_num = args.ns
minimum_similarity = args.ms
number_baits_desired = args.nb
reference = args.r

################ Functions*
def all_equal(items):
    #Returns True iff all items are equal
    first = items[0]
    return all(x == first for x in items)

def compute_match(aligned_sequences):
    #Returns the ratio of same-character columns in ``aligned_sequences``.
    #:param aligned_sequences: a list of strings or equal length.
    match_count = 0
    mismatch_count = 0
    for chars in zip(*aligned_sequences):
        # Here chars is a column of chars, 
        # one taken from each element of aligned_sequences.
        if all_equal(chars):
            match_count += 1
        else:
            mismatch_count += 1
    return (float(match_count) / len(aligned_sequences[0])) * 100
################ *special thanks to stackoverflow user 9000 who helped make this code faster 

start_time = time.time()

os.chdir("./MAFFT_ADD_REF_ALIGN_FASTA")
files = glob.glob('*.fna')

columns  = ["marker","predicted_length","number_of_introns","average_intron_lenght","total_introns_lenght",
"number_of_exons","average_exon_lenght","total_exons_lengt","percentage_short_exons",
"number_sequences","marker_length","total_nucleotides","global_identity","baits_in_maker"]
stats = pd.DataFrame(columns=columns)

ref_pattern = '^' + reference

for file in files:
    for record in SeqIO.parse(file, "fasta"):
        if re.match(ref_pattern, record.name):
            name = str(record.name)
            print "(ᵔᴥᵔ) – processing (part I of II) " + name
            seq = str(record.seq)
            introns_length = seq.count('n')                             #count the total lenght of introns
            introns = list(filter(None, re.split('a|c|t|g|-', seq)))    #get a list of intros
            number_introns = len(introns)                               #count the number of introns
            if number_introns != 0:
                intron_length_mean = introns_length / number_introns
            else:
                intron_length_mean = 0
            #print "average intron size " + str(intron_length_mean)
            
            exons = list(filter(None, re.split('n', seq)))              #get a list of exons
            exons = map(lambda each:each.replace('-',''), exons)        #remove the gaps in the exons
            number_exons = len(exons)                                   #count the number of exons
            exons_length_each = map(len, exons)
            exons_length = sum(exons_length_each)
            exon_length_mean = exons_length / number_exons
            number_short_exons = sum(i < 120 for i in exons_length_each)
            if number_exons !=0:
                perc_short_exons = float(number_short_exons) / float(number_exons) * 100
            else:
                perc_short_exons = 0
            
            predicted_length = introns_length + exons_length
            stats = stats.append({"marker":name,"predicted_length":predicted_length,"number_of_introns":number_introns,"average_intron_lenght":intron_length_mean,
            "total_intons_lenght":introns_length,"number_of_exons":number_exons,"average_exon_lenght":exon_length_mean,
            "total_exon_lengt":exons_length,"percentage_short_exons":perc_short_exons}, ignore_index=True)

stats.head
stats.set_index('marker', inplace=True)

os.chdir("../MAFFT_NUC_ALIGN_FASTA")
files = glob.glob('*.fna')

for file in files:
    align = AlignIO.read(file,format= "fasta")
    marker = file.split(".")[0]
    print "(ᵔᴥᵔ) – processing (part II of II) " + marker
    seqs = []
    for record in align:
        seq = str(record.seq)
        seq = str(seq)
        seqs.append(seq)
    stripped_seqs = map(lambda each:each.replace('-',''), seqs)
    seq_len_each = map(len, stripped_seqs)
    total_nuc = sum(seq_len_each)
    number_seqs = len(seq_len_each)
    marker_length = total_nuc / number_seqs
    baits_in_marker1 = total_nuc / (bait_length / bait_coverage)
    stats.loc[marker,'number_sequences'] = number_seqs
    stats.loc[marker,'total_nucleotides'] = total_nuc
    stats.loc[marker, 'marker_length'] = marker_length
    stats.loc[marker, 'baits_in_maker'] = baits_in_marker1
    if len(align) != 1:
        global_identity = compute_match(align)
        stats.loc[marker,'global_identity'] = global_identity
        #print(global_identity)

os.chdir("..")

stats.to_csv(path_or_buf="intron_exon_stats.csv")

################## This code reads "intron_exon_stats.csv"
#stats = pd.read_csv("intron_exon_stats.csv")
#stats.set_index('marker', inplace=True)
#stats.head(n=5)
#len(stats)
################## should only be un-commented for debugging

#Lets sort the dataframe
stats = stats.sort_values(by="marker_length", ascending=False)
stats.head(n=5)

#Subselect for a minimum marker lenght
best_markers0 = stats.loc[stats['marker_length'] >= minimum_marker_lenght] 

#Subselect for a minium number of number of sequences
best_markers1 = best_markers0.loc[stats['number_sequences'] >= seq_num] 
best_markers1.head(n=5)
len(best_markers1)

#Now let's remove markers that contains too many exons less of 120bp
best_markers2 = best_markers1.loc[best_markers1['percentage_short_exons'] <= perc_short_exons_desired] 
best_markers2.head(n=5)
len(best_markers2)

#Keep only the markes in which the desired minimum similarity (based on the alignment)
best_markers3 = best_markers2.loc[best_markers2['global_identity'] >= minimum_similarity] 
best_markers3.head(n=5)
len(best_markers3)

#Check if selection has come to zero markers
if len(best_markers3) == 0:
    print("no marker fits your input parameters, be less strict ¯\_(ツ)_/¯")
    sys.exit(0)

#calculate total number of baits of initial selection
total_baits = sum(best_markers3['baits_in_maker'])

#select the number of markers topped by a maximum numnber of baits
if number_baits_desired < total_baits:
    baits=0
    baits_per_maker=[]
    for index, row in best_markers3.iterrows():
        if int(baits) <= number_baits_desired:
            baits_in_marker = int(row['total_nucleotides'] / (bait_length / bait_coverage))
            baits_per_maker.append(baits_in_marker)
            baits = sum(baits_per_maker)
    best_markers4 = best_markers3.iloc[:(len(baits_per_maker)-1)]
else:
    best_markers4 = best_markers3

#calculate the number of baits on final selection
number_baits = sum(best_markers4['baits_in_maker'])

#now caculate how big would be the supermatrix
supermatrix = sum(best_markers4['predicted_length'])

#lets calculate the average for marker length
average_marker_length = sum(best_markers4['marker_length']) / len(best_markers4)

#save the table with the inormation of the markers that were subselected
best_markers4.to_csv(path_or_buf="golden_markers.csv")

#make a list of markers
markers = list(best_markers4.index.values)

####    Create folders with subselections    ####
####                                         ####
#copy files from folder containing aligmnets against reference

if len(glob.glob("*SUBSELECTION")) >= 4:
    shutil.rmtree("MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION")
    shutil.rmtree("MAFFT_NUC_ALIGN_FASTA_SUBSELECTION")
    shutil.rmtree("MAFFT_NUC_ALIGN_PHY_SUBSELECTION")
    shutil.rmtree("NUC_FASTA_SUBSELECTION")

os.makedirs("MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION")
os.makedirs("MAFFT_NUC_ALIGN_FASTA_SUBSELECTION")
os.makedirs("MAFFT_NUC_ALIGN_PHY_SUBSELECTION")
os.makedirs("NUC_FASTA_SUBSELECTION")

#copy ref-aligments of selected markers 
os.chdir("./MAFFT_ADD_REF_ALIGN_FASTA")
files2 = glob.glob('*.fna')

for file in files2:
    prefix = file.split(".")[0]
    if prefix in markers:
        shutil.copy(file, "../MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION/")
os.chdir('..')

#copy files from folder containing FASTA alignments
os.chdir("./MAFFT_NUC_ALIGN_FASTA")
files3 = glob.glob('*.fna')

for file in files3:
    prefix = file.split(".")[0]
    if prefix in markers:
        shutil.copy(file, "../MAFFT_NUC_ALIGN_FASTA_SUBSELECTION/")
os.chdir('..')

#copy files from folder containing PHYLIP alignments
os.chdir("./MAFFT_NUC_ALIGN_PHY")
files4 = glob.glob('*.phy')

for file in files4:
    prefix = file.split(".")[0]
    if prefix in markers:
        shutil.copy(file, "../MAFFT_NUC_ALIGN_PHY_SUBSELECTION/")
os.chdir('..')

#copy subselected files from folder containing the sequences not aligned
os.chdir("./NUC_FASTA")
files4 = glob.glob('*.fna')

for file in files4:
    prefix = file.split(".")[0]
    if prefix in markers:
        shutil.copy(file, "../NUC_FASTA_SUBSELECTION/")
os.chdir('..')


####Print results

print("ʕ•ᴥ•ʔ the subselection took %s seconds" % (time.time() - start_time))
print("ʕᵔᴥᵔʔ the subselection found %s makers" % len(best_markers4))
print("ʕ•ᴥ•ʔ the average size per marker is %s" % average_marker_length)
print("ʕᵔᴥᵔʔ your selection contains %s baits" % number_baits)



#Script to split the output of MarkerMiner into exons for bait design

import os
from Bio import AlignIO
import re

os.makedirs("MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION_EXONS")
folder = "./MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION_EXONS"
file = ("./MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION/AT1G01880.mafft.added.reference.align.fna")
align  = AlignIO.read(file,format= "fasta")
ref_pattern = "AT"

print(align)
print align[:,2:5]

for record in align:
    if re.match(ref_pattern, str(record.name)):
        exons = list(filter(None, re.split('n', str(record.seq))))
        number_exons = len(exons) 
        introns = list(filter(None, re.split('a|c|t|g|-', str(record.seq))))
        number_introns = len(introns)
        number_modules = number_exons + number_introns
        print record.name
        print number_exons
        print number_introns
        print number_modules
        coordinates = [0,len(exons[0])]
        if number_exons > 1:
            for i in range(0,number_exons-1):
                coordinates += [coordinates[-1]+len(introns[i])+1, coordinates[-1]+len(introns[i])+len(exons[i+1])]
                print coordinates
        for i in range(0, number_exons):
            print "\n aling for coordinates " + str(coordinates[i*2]) + " to " + str(coordinates[(i*2)+1])
            exon_align = align[:-1,coordinates[i*2]:coordinates[(i*2)+1]]
            print str(i+1)
            os.chdir("./MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION_EXONS")
            file_name = str(record.name) + ".E" + str(i+1) + ".mafft.added.reference.align.fna"
            output_handle = open(file_name, "w")
            AlignIO.write(exon_align, output_handle, "fasta")
            output_handle.close()
            os.chdir('..')
            print file_name



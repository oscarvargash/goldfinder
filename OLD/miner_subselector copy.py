# Python script to subselect the best markers produced by the output of MarkerMiner
# Written by Oscar Vargas oscarmvargas.org

import os
from Bio import SeqIO
from Bio import AlignIO
import pandas as pd
import re
import glob
import shutil

################ Functions*
def all_equal(items):
    """Returns True iff all items are equal."""
    first = items[0]
    return all(x == first for x in items)

def compute_match(aligned_sequences):
    """Returns the ratio of same-character columns in ``aligned_sequences``.

    :param aligned_sequences: a list of strings or equal length.
    """
    match_count = 0
    mismatch_count = 0
    for chars in zip(*aligned_sequences):
        # Here chars is a column of chars, 
        # one taken from each element of aligned_sequences.
        if all_equal(chars):
            match_count += 1
        else:
            mismatch_count += 1
    return (float(match_count) / len(aligned_sequences[0])) * 100
################ *special thanks to user 9000 who helped make this code faster https://stackoverflow.com/questions/49241130/faster-way-of-calculating-the-percentage-of-identical-sites-in-alignment-using-b?noredirect=1#comment85490340_49241130

start_time = time.time()

os.chdir("./MAFFT_ADD_REF_ALIGN_FASTA")
files = glob.glob('*.fna')

columns  = ["marker","predicted_length","number_of_introns","average_intron_lenght","total_intons_lenght",
"number_of_exons","average_exon_lenght","total_exon_lengt","percentage_short_exons",
"number_sequences","total_nucleotides","global_identity"]
stats = pd.DataFrame(columns=columns)

for file in files:
    for record in SeqIO.parse(file, "fasta"):
        if re.search('^AT', record.name):
            name = str(record.name)
            print "(ᵔᴥᵔ) – processing (part I of II) " + name
            seq = str(record.seq)
            introns_length = seq.count('n')                             #count the total lenght of introns
            introns = list(filter(None, re.split('a|c|t|g|-', seq)))    #get a list of intros
            number_introns = len(introns)                               #count the number of introns
            if number_introns != 0:
                intron_length_mean = introns_length / number_introns
            else:
                intron_length_mean = 0
            #print "average intron size " + str(intron_length_mean)
            
            exons = list(filter(None, re.split('n', seq)))              #get a list of exons
            exons = map(lambda each:each.replace('-',''), exons)        #remove the gaps in the exons
            number_exons = len(exons)                                   #count the number of exons
            exons_length_each = map(len, exons)
            exons_length = sum(exons_length_each)
            exon_length_mean = exons_length / number_exons
            number_short_exons = sum(i < 120 for i in exons_length_each)
            if number_exons !=0:
                perc_short_exons = float(number_short_exons) / float(number_exons) * 100
            else:
                perc_short_exons = 0
            
            predicted_length = introns_length + exons_length
            #print "there are " + str(number_exons) + " exons with a average lenght of " + str(exon_length_mean)
            #print str(number_short_exons) + " exons are under 120 bp"
            stats = stats.append({"marker":name,"predicted_length":predicted_length,"number_of_introns":number_introns,"average_intron_lenght":intron_length_mean,
            "total_intons_lenght":introns_length,"number_of_exons":number_exons,"average_exon_lenght":exon_length_mean,
            "total_exon_lengt":exons_length,"percentage_short_exons":perc_short_exons}, ignore_index=True)

stats.head
stats.set_index('marker', inplace=True)

os.chdir("../MAFFT_NUC_ALIGN_FASTA")
files = glob.glob('*.fna')

for file in files:
    align = AlignIO.read(file,format= "fasta")
    marker = file.split(".")[0]
    print "(ᵔᴥᵔ) – processing (part II of II) " + marker
    seqs = []
    for record in align:
        seq = str(record.seq)
        seq = str(seq)
        seqs.append(seq)
    stripped_seqs = map(lambda each:each.replace('-',''), seqs)
    seq_len_each = map(len, stripped_seqs)
    total_nuc = sum(seq_len_each)
    number_seqs = len(seq_len_each)
    stats.loc[marker,'number_sequences'] = number_seqs
    stats.loc[marker,'total_nucleotides'] = total_nuc
    
    if len(align) != 1:
        global_identity = compute_match(align)
        stats.loc[marker,'global_identity'] = global_identity
        #print(global_identity)


os.chdir("..")

#stats.to_csv(path_or_buf="intron_exon_stats.csv")

################## This code reads "intron_exon_stats.csv"
#stats = pd.read_csv("intron_exon_stats.csv")
#stats.set_index('marker', inplace=True)
#stats.head(n=5)
#len(stats)
################## should only be un-commented 

#First subselect for a minium number of sequences of 2
best_markers1 = stats.loc[stats['number_sequences'] >= 2] 
best_markers1.head(n=5)
len(best_markers1)

#Now let's remove markers that contains too many exons less of 120bp (<30%)
best_markers2 = best_markers1.loc[best_markers1['percentage_short_exons'] <= 30] 
best_markers2.head(n=5)
len(best_markers2)

#Finally let's keep only the markes in which the glolbal identity is > 60%

best_markers3 = best_markers2.loc[best_markers2['global_identity'] >= 60] 
best_markers3.head(n=5)
len(best_markers3)

#let's calculate the number of baits 
number_baits = sum(best_markers3['total_nucleotides']) / 60

#now caculate how big would be the supermatrix
supermatrix = sum(best_markers3['predicted_length'])

#save the table with the inormation of the markers that were subselected
#best_markers3.to_csv(path_or_buf="best_markers.csv")

markers = list(best_markers3.index.values)

####    Create Folder with subselections    ####
####                                        ####

##copy files from folder containing aligmnets against reference
#os.makedirs("MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION")
#os.chdir("./MAFFT_ADD_REF_ALIGN_FASTA")
#files2 = glob.glob('*.fna')
#
#for file in files2:
#    prefix = file.split(".")[0]
#    if prefix in markers:
#        shutil.copy(file, "../MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION/")
#    #print (prefix)
#os.chdir('..')
#
##copy files from folder containing FASTA alignments
#os.makedirs("MAFFT_NUC_ALIGN_FASTA_SUBSELECTION")
#os.chdir("./MAFFT_NUC_ALIGN_FASTA")
#files3 = glob.glob('*.fna')
#
#for file in files3:
#    prefix = file.split(".")[0]
#    if prefix in markers:
#        shutil.copy(file, "../MAFFT_NUC_ALIGN_FASTA_SUBSELECTION/")
#    print (prefix)
#os.chdir('..')
#
##copy files from folder containing PHYLIP alignments
#os.makedirs("MAFFT_NUC_ALIGN_PHY_SUBSELECTION")
#os.chdir("./MAFFT_NUC_ALIGN_PHY")
#files4 = glob.glob('*.phy')
#
#for file in files4:
#    prefix = file.split(".")[0]
#    if prefix in markers:
#        shutil.copy(file, "../MAFFT_NUC_ALIGN_PHY_SUBSELECTION/")
#    print (prefix)
#os.chdir('..')


print("ʕ•ᴥ•ʔ the subselection took %s seconds" % (time.time() - start_time))
print("ʕᵔᴥᵔʔ the subselection found %s makers" % len(best_markers3))




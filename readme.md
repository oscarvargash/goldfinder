# GoldFinder #

This is the repository for GoldFinder, a script that will help you subselect the locus produced by MarkerMiner for further sequencing.

Vargas OM, Heuertz M, Smith SA, Dick CW. 2019. Target sequence capture in the Brazil nut family (Lecythidaceae): marker selection and in silico capture from genome skimming data. Molecular Phylogenetics and Evolution 135: 98–104 [https://doi.org/10.1016/j.ympev.2019.02.020](https://doi.org/10.1016/j.ympev.2019.02.020)

GoldFinder is designed to read the output aligments located in `MAFFT_ADD_REF_ALIGN_FASTA` and `MAFFT_NUC_ALIGN_FASTA` to stimate different metrics about the markers and then (with these metrics) subselect the best makers for sequencing.

GoldFinder will only works if the reference you used in MarkerMiner contains the introns masked with "n"s. The in-house databases of MarkerMiner contain the introns masked, meaning that this script would work inf you used one the references provided by MarkerMiner.

GoldFinder "goldfinder.py" contains a set arguments, that would subselect the locus produced by MarkerMiner, which are usually over a thousand, into a sunset of best loci that best matches your criteria. The pogram usually runs in less than 20 seconds in a dataset of ~2000 loci.

`python goldfinder.py -h`

would print the following help:
```
usage: goldfinder.py [-h] [-bl BAIT_LENGHT] [-bc BAIT_COVERAGE]
                     [-ml MINIMUM_LENGTH] [-pse PERCENTAGE_SHORT_EXONS]
                     [-ns NUMBER_SAMPLES] [-ms MINIMUM_SIMILARITY]
                     [-nb NUMBER_BAITS] [-r REFERENCE]

Script to subselect the best markers produced by the output of MarkerMiner, to
run the program you need to run it from the the folder with the results of
MarkerMiner: the folder containing single_copy_genes.txt and all the folders
with aligments. Runing the code without arguments will run the program with
all the defaults. The program will always overwrite your results

optional arguments:
  -h, --help            show this help message and exit
  -bl BAIT_LENGHT, --bait-lenght BAIT_LENGHT
                        length of baits, default=120
  -bc BAIT_COVERAGE, --bait-coverage BAIT_COVERAGE
                        converage of baits, default=2
  -ml MINIMUM_LENGTH, --minimum-length MINIMUM_LENGTH
                        minimum lenght of marker, default=900
  -pse PERCENTAGE_SHORT_EXONS, --percentage-short-exons PERCENTAGE_SHORT_EXONS
                        percentage of short exons allowed, default=30
  -ns NUMBER_SAMPLES, --number-samples NUMBER_SAMPLES
                        minimum number of samples with sequences in marker
                        aligment, default=2
  -ms MINIMUM_SIMILARITY, --minimum-similarity MINIMUM_SIMILARITY
                        minimum (percentage) similarity among sequences in
                        alignment, default=60
  -nb NUMBER_BAITS, --number-baits NUMBER_BAITS
                        number of baits desired, default=30000
  -r REFERENCE, --reference REFERENCE
                        reference used in MarkerMiner, default=AT, available:
                        AL,AT,BD,CP,FV,GM,MD,ME,MT,OS,PT,RC,SB,TC,VV,ZM. If
                        you used your own reference, plese add the two first
                        letters found in every name sequence in your reference
```

Then, if for example you want to make a search of only markers that are longer than 1000 bp and you want to have up to 10000 baits in your essay you would type

`python goldfinder.py -ml 1000 -nb 10000`

The outputs in the repository are the result of:

`python goldfinder.py -pse 0`

GoldFinder produces the following outputs:

* `intron_exon_stats.csv`  contains statistics for all the markers found by MarkerMiner
* `golden_markers.csv` contains statistics for the "golden" markerd found by GoldFinder
* `*_SUBSELECTION` (3) folders containing the aligments of the sublelected markerd by GoldFinder
* `MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION_EXONS` and `MAFFT_ALIGN_FASTA_SUBSELECTION_EXONS`contain aligments for every exon, with and without a reference, respectively. The purpose of these files is to better desing baits so they "fit" to the edges of the exons, avoiding baits to span over introns. WARNING: The accuracy of the exon splitting in these files depends entirely on the the alignments with the reference `MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION`. Being that said, if the reference is too far phylogenetically, the  boundaries of the exons might not align perfectly between the target taxa and the reference, making the splitting imperfect. However imperfect this exon splitting is, I believe it is a step forward to desing the baits directly on the transcripts which will result in baits spanning over introns.


# How to use this data for Lecythidaceae bait desing #

The alignments used to design the baits are located in this folder:

https://bitbucket.org/oscarvargash/goldfinder/src/master/MAFFT_ADD_ALIGN_FASTA_SUBSELECTION_EXONS/

The first part of the file name before the period, for example AT1G01180, is a code for a gene based on Arabidopsis reference. 

The second part, E1, indicates the index for the exon.

Please be aware that there were some exons that need to be removed because they were found to possibly have duplicates in the genome. The exons to keep are marked as “keep” in the file:

Drop_exons.xls

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
this script will read a csv file with a table that contains a list of exons
it will put in new folder a subselection with the exons to keep
"""

import pandas as pd
import glob
import os
import shutil

table = pd.read_csv("Drop_exons.csv")

keep_markers = table.loc[table["action"] == "Keep"]
keep_markers_list = list(keep_markers["id"])

os.makedirs("MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION_EXONS_longest_keep")

os.chdir("./MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION_EXONS_longest")
files = glob.glob('*.longest')

for exon in keep_markers_list:
    for file in files:
        if exon + '.' in file:
            shutil.copy(file, "../MAFFT_ADD_REF_ALIGN_FASTA_SUBSELECTION_EXONS_longest_keep/")
            print "copying " + file

os.chdir('..')